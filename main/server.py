#!/usr/bin/env python3
# -- coding: utf-8 --

# File name: server.py
# Description: Script to handle the server code
# Author: Avinash
# Date: 30-06-2021
import os
import sys
sys.path.append('./')

import pytz
from datetime import datetime, timezone
from flask import jsonify
print(os.getcwd())
from dbase.db_connector import load_config, RedisDB, MongoDB
from logger_conf.logger import get_logger
import json
from flask import Flask, request
import requests
import time
#import sys
#sys.path.append('./')


utc_dt = datetime.now(timezone.utc)
IST = pytz.timezone('Asia/Kolkata')

logger = get_logger(__name__)

app = Flask(__name__)


print(os.environ)

try:
    # create connections to redis and mongo
    redis_conn = RedisDB()
    mongo_conn = MongoDB()
    bot_config = load_config()

    with open("configs/utter_messages.json", "r", encoding="utf-8") as fa:
        messages = json.load(fa)

except Exception as e:
    logger.exception("Exception in loading config"+str(e))


@app.route("/webhook/action_ask_name", methods=["POST"])
def get_response_action_name():
    response_text = {}
    try:
        start = time.time()
        # Extract all parameters
        logger.info("=" * 10)
        data = json.loads(request.data.decode('utf-8'))
        logger.info("request recieved is : "+str(data))

        # Check if the channel is voice
        # If channel is voice we can extract phone number and fetch details
        # from Db
        if data["details"]["channel"] == "nlpio":
            q1 = {"phone_number": int(data["details"]["phone_number"])}
            cust_info = mongo_conn.mongo_get_collection_data("input", q1)

            full_name = str(cust_info["full_name"])

        else:
            # For other channels sending default amount
            # Only if we can extract mobile number we can change it to dynamic
            full_name = "James Bond"
        response_message = messages["utter_info_talk"].replace(
            "<full_name>", full_name)
        current_response = {"text":response_message}

        responses = []
        responses.append(current_response)

        response_text["responses"] = responses
        response_text["conversation_instances"] = []
        logger.info("response is : ",response_text)

    except Exception as e:
        logger.exception("Exception occured in the webhook endpoint : "+str(e))
        #response_text = modify_response(sender_id,messages["utter_fallback"])
        #response_text = jsonify(response_text)

    end = time.time()
    logger.info("response is {} and response time : {} ".format(
        response_text, str(end-start)))
    response_text = jsonify(response_text)
    return response_text


@app.route("/webhook/action_ask_city", methods=["POST"])
def get_response_action_city():
    response_text = {}
    try:
        start = time.time()
        # Extract all parameters
        logger.info("=" * 10)
        data = json.loads(request.data.decode('utf-8'))
        logger.info("request recieved is : "+str(data))

        # Check if the channel is voice
        # If channel is voice we can extract phone number and fetch details
        # from Db
        if data["details"]["channel"] == "nlpio":
            q1 = {"phone_number": int(data["details"]["phone_number"])}
            cust_info = mongo_conn.mongo_get_collection_data("input", q1)

            city = str(cust_info["city"])

        else:
            # For other channels sending default amount
            # Only if we can extract mobile number we can change it to dynamic
            city = "Bengaluru"
        response_message = messages["utter_city"].replace(
            "<city>", city)
        current_response = {"text":response_message}

        responses = []
        responses.append(current_response)

        response_text["responses"] = responses
        response_text["conversation_instances"] = []
        logger.info("response is : ",response_text)

    except Exception as e:
        logger.exception("Exception occured in the webhook endpoint : "+str(e))
        #response_text = modify_response(sender_id,messages["utter_fallback"])
        #response_text = jsonify(response_text)

    end = time.time()
    logger.info("response is {} and response time : {} ".format(
        response_text, str(end-start)))
    response_text = jsonify(response_text)
    return response_text


@app.route("/webhook/action_ask_address", methods=["POST"])
def get_response_action_address():
    response_text = {}
    try:
        start = time.time()
        # Extract all parameters
        logger.info("=" * 10)
        data = json.loads(request.data.decode('utf-8'))
        logger.info("request recieved is : "+str(data))

        # Check if the channel is voice
        # If channel is voice we can extract phone number and fetch details
        # from Db
        if data["details"]["channel"] == "nlpio":
            q1 = {"phone_number": int(data["details"]["phone_number"])}
            cust_info = mongo_conn.mongo_get_collection_data("input", q1)

            address = str(cust_info["address"])

        else:
            # For other channels sending default amount
            # Only if we can extract mobile number we can change it to dynamic
            city = "Lalbagh WestGate, Jaynagar"
        response_message = messages["utter_address"].replace(
            "<address>", address)
        current_response = {"text":response_message}

        responses = []
        responses.append(current_response)

        response_text["responses"] = responses
        response_text["conversation_instances"] = []
        logger.info("response is : ",response_text)

    except Exception as e:
        logger.exception("Exception occured in the webhook endpoint : "+str(e))
        #response_text = modify_response(sender_id,messages["utter_fallback"])
        #response_text = jsonify(response_text)

    end = time.time()
    logger.info("response is {} and response time : {} ".format(
        response_text, str(end-start)))
    response_text = jsonify(response_text)
    return response_text


if __name__ == '__main__':
    app.secret_key = "123"
    app.run(host='0.0.0.0', port=5002)
