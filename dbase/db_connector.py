# !/usr/bin/env python3
# -- coding: utf-8 --

# File name: tracker_store_connection.py
# Description: DB connection layer
# Author: Avinash
# Date: 18-06-2021

import sys
sys.path.append('../')

import os
import redis
import yaml
import pickle
from logger_conf.logger import get_logger
from pymongo import MongoClient
from pymongo import ReadPreference

# name of the module for logging 
logger = get_logger(__name__)

def load_config():
    conf = ""
    try:
        environment = os.environ["BOT_ENV"]
        logger.info("Bot Environment is :"+str(environment))
        if environment == "prod":
            endpoint_filename = "endpoints.yml"                
        else:
            endpoint_filename = "endpoints_dev.yml" 
        with open('configs/'+endpoint_filename, 'r') as f:
            conf = yaml.load(f, Loader=yaml.FullLoader) 
    
    except Exception as e:
        logger.exception("Error loading the configuration from the endpoints.yml "+str(e))
    return conf


def load_domain():
    conf = ""
    try:
        endpoint_filename = "configs/domain.yml"
        with open(endpoint_filename, 'r') as f:
            conf = yaml.load(f, Loader=yaml.FullLoader)

    except Exception as e:
        logger.exception("Error loading the configuration from the domain.yml " + str(e))
    return conf


# -------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------
class MongoDB:

    def __init__(self):

        try:
            self._database_config = load_config()
            environment = self._database_config["server"]["environment"]
            # mongo_host = self._database_config["mongo_database"]["url"]
            # mongo_port = self._database_config["mongo_database"]["port"]
            mongo_username = self._database_config["mongo_database"]["username"]
            mongo_password = self._database_config["mongo_database"]["password"]
            mongo_auth_src = self._database_config["mongo_database"]["authSource"]
            self._mongo_db = self._database_config["mongo_database"]["db_name"]

            if environment == "prod":

                # Read the variables from the env variables
                mongo_port = os.environ["mongo_port"]
                mongo_host1 = os.environ["mongo_host0"] + ":" + mongo_port
                mongo_host2 = os.environ["mongo_host1"] + ":" + mongo_port
                mongo_host3 = os.environ["mongo_host2"] + ":" + mongo_port
                mongo_replica_set = os.environ["mongo_replica_set"]

                logger.debug("Host Details for Mongo")
                logger.debug("Mongo Host 1:" + str(mongo_host1))
                logger.debug("Mongo Host 2:" + str(mongo_host2))
                logger.debug("Mongo Host 3:" + str(mongo_host3))

                mongo_url = "mongodb://{0}:{1}@{2},{3},{4}".format(mongo_username, \
                                                                   mongo_password, mongo_host1, mongo_host2,
                                                                   mongo_host3)

            else:

                mongo_host = self._database_config["mongo_database"]["url"]
                mongo_port = self._database_config["mongo_database"]["port"]

                mongo_url = "mongodb://" + str(mongo_username) + ":" + str(mongo_password) + "@" + str(
                    mongo_host) + ":" + str(mongo_port)

            try:
                if mongo_url is not None:
                    if environment == "prod":
                        self._conn = MongoClient(mongo_url, authSource=mongo_auth_src,
                                                 replicaSet=str(mongo_replica_set),
                                                 read_preference=ReadPreference.PRIMARY)
                    else:
                        self._conn = MongoClient(mongo_url, authSource=mongo_auth_src)

                    if self._conn is not None:
                        logger.info("Connected to the database")
                    else:
                        logger.error("Could not connect to the Mongo URL !!!")
                else:
                    logger.error("Problem when fetching the database details from endpoints.yml !!!")
            except BaseException as e:
                logger.info("ERROR!!! When trying to connect to Mongo Database : " + str(e))

            logger.debug("Connecting to mongo input collection")
            self.mongo_input_collection = self._database_config["mongo_database"]["input_collection"]
            self.input_col = self._conn[self._mongo_db][self.mongo_input_collection]

            logger.debug("Connecting to mongo output collection")
            self.mongo_output_collection = self._database_config["mongo_database"]["output_collection"]
            self.output_col = self._conn[self._mongo_db][self.mongo_output_collection]

            logger.debug("Connecting to mongo sms collection")
            self.sms_col = self._conn[self._mongo_db]["sms_report"]

        except Exception as e:
            logger.exception("Exception while loading the config-->" + str(e))

    def mongo_file_count(self, col_type="", q1=""):
        ''' This function will return the count of the record present in collection'''

        try:

            if col_type == "input":
                COL = self.input_col
            elif col_type == "output" or col_type == "report":
                COL = self.output_col
            else:
                logger.info("Please mention the collection type input or output")
                exit()
            print("query dict is --> ", q1)
            record_count = COL.find(q1).count()

            return record_count

        except Exception as e:
            logger.exception("Exception in mongo_file_count function--> " + str(e))
            return ""

    def mongo_delete(self, col_type="", q1={"phone_number": 11111}):
        ''' This function will return the true or false based on whether the record is deleted or not'''

        try:
            if col_type == "input":
                INPUT_COL = self.input_col
            elif col_type == "output" or col_type == "report":
                INPUT_COL = self.output_col
            else:
                logger.info("Please mention the collection type input or output")
                exit()

                # before deleting, check to see if the data for that condition is present
            check = self.mongo_file_count(col_type=col_type, q1=q1)
            if check >= 1:

                INPUT_COL = self.input_col

                print("m_delete function: query dict is --> ", q1)
                INPUT_COL.delete_many(q1)
                return True
            else:
                logger.info("Record not found for condition -> " + str(q1))
                return False
        except Exception as e:
            logger.exception("Exception in m_delete function -->" + str(e))
            return False

    # mode => insert or update ; record-> to insert or set fields; upd_cond-> In case of update condition
    # col_type -> input collection or output collection
    def mongo_update(self, col_type="", mode="", record={}, upd_cond={"phone_number": 11111}):
        ''' This function will return the true or false based on whether insert/update was performed on the database'''

        try:

            if col_type == "input":
                COL = self.input_col
            elif col_type == "output" or col_type == "report":
                COL = self.output_col
            elif col_type == "sms":
                COL = self.sms_col
            else:
                logger.info("Please mention the collection type input or output")
                exit()

            if "_id" in record:
                del record['_id']
            if mode == "insert":
                logger.info("Inserting the record")
                COL.insert_one(record)
                return True
            elif mode == "update":
                logger.info("Updating the record")
                logger.info(" Update condition is --> " + str(upd_cond))
                COL.update_one(upd_cond, {"$set": record})
                return True
            else:
                logger.info("Please mention the mode: NO UPDATION OR INSERTION HAPPENED ")
                return False

        except Exception as e:
            logger.exception("Exception in mongo_update function-->" + str(e))
            return False

    # col type => input or output ; q1 -> filter rows query ; q2-> filter column query if present
    def mongo_get_collection_data(self, col_type, q1, q2={"_id": 0}):
        '''this function will return the document from the collection'''

        try:

            if col_type == "input":
                COL = self.input_col
            elif col_type == "output" or col_type == "report":
                COL = self.output_col
            else:
                logger.info("Please mention the collection type input or output")
                exit()

            # logger.info("query dict is :"+ str(query_dict))
            input_collection = COL.find_one(q1, q2)
            return input_collection
        except Exception as e:
            logger.exception("Exception in mongo_get_collection_data --> " + str(e))
            return ""

    def mongo_get_all_collection_data(self, col_type, q1, q2={"_id": 0}):
        '''this function will return the document from the collection'''

        try:

            if col_type == "input":
                COL = self.input_col
            elif col_type == "output" or col_type == "report":
                COL = self.output_col
            else:
                logger.info("Please mention the collection type input or output")
                exit()

            # logger.info("query dict is :"+ str(query_dict))
            input_collection = COL.find(q1, q2)
            return input_collection
        except Exception as e:
            logger.exception("Exception in mongo_get_collection_data --> " + str(e))
            return ""

    def mongo_add_required_keys(self, input_data):

        reset_keys = {
            'conversation_log': [],
            'tracker_log': [],
            'fallback_count': 0,
            'action': '',

        }

        required_keys = {
            "call_sequence_mapping": {},

        }

        for field, value in reset_keys.items():
            input_data[field] = value

        for field, value in required_keys.items():
            if field not in input_data:
                input_data["field"] = value

        logger.info("Final Data after: " + str(input_data))
        return input_data


# -----------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------
class RedisDB:

    def __init__(self):

        try:
            self._redis_config = load_config()
            environment = self._redis_config["server"]["environment"]
            if environment == "prod":
                # Reading from environment variables in case of Prod
                redis_host = os.environ["redis_host"]
                redis_port = os.environ["redis_port"]
            else:
                redis_host = self._redis_config["redis_tracker_store"]["url"]
                redis_port = self._redis_config["redis_tracker_store"]["port"]

            db_number = self._redis_config["redis_tracker_store"]["db_mapping"]
            db_tracker_num = self._redis_config["redis_tracker_store"]["db_tracker"]

            if (redis_host is not None) and (redis_port is not None):
                self._redis_conn = redis.StrictRedis(host=redis_host, port=redis_port, db=db_number)
                self._redis_track_conn = redis.StrictRedis(host=redis_host, port=redis_port, db=db_tracker_num)
                if self._redis_conn is not None and self._redis_track_conn is not None:
                    logger.info("Successfully Connected to the Redis Database!!!")
                else:
                    logger.error("Unable to connect Redis Database!!!")
            else:
                logger.error("Problem when fetching the Redis details from endpoints.yml !!!")
        except BaseException as e:
            logger.exception("ERROR!!! When Connecting to the Redis Database :  " + str(e))
            self._redis_conn = None
            self._redis_track_conn = None

    def redis_delete_tracker(self, key):
        """function to delete tracker in redis. 

        Args:
            key (string): key for which value has to delete

         Returns:
            integer: 0 or 1 based on whether key was deleted from redis or not
        """
        try:
            logger.info("Entered redis_delete_tracker function")
            value = self._redis_track_conn.delete(key)
            logger.info("Exit redis_delete_tracker function")
            return value
        except BaseException as e:
            logger.exception("Unable to DELETE value and expiry  for key : " + key + " ERROR is : " + str(e))

    def redis_get_value(self, key):
        """function to get value in string form of a key from redis

        Args:
            key (string): key whose value has to be fetched

        Returns:
            string: value corresponding to the key set in redis
        """
        try:
            value = self._redis_conn.get(key)
            if value is None:
                return value
            else:
                return value.decode('utf-8')
        except BaseException as e:
            logger.exception("Unable to GET value for key : " + key + " ERROR is : " + str(e))

    def redis_get_dict_value(self, key):
        """function to retrieve value which is in dictionary format for a given key in redis

        Args:
            key (string): Key whose value has to be fetched from redis

        Returns:
            dictionary: corresponding dictionary value of the key from redis
        """
        try:
            value = self._redis_conn.get(key)
            if value is None:
                return value
            else:
                return pickle.loads(value)
        except BaseException as e:
            logger.exception("Unable to GET value for key : " + key + " ERROR is : " + str(e))

    def redis_set_value(self, key, value):
        """function to set a value for the key in redis. 
        Use this function if you want to set a string value to a key

        Args:
            key (string): key for which value has to be set
            value (string): value corresponding to the key

        Returns:
            integer: 0 or 1 based on value was set to corresponding key or not
        """
        try:
            value = self._redis_conn.set(key, value)
            return value
        except BaseException as e:
            logger.exception("Unable to SET value for key : " + key + " ERROR is : " + str(e))

    def redis_set_dict_value(self, key, value):
        """function to set a value for the key in redis. 
        Use this function if you want to set a dictionary value to a key

        Args:
            key (string): key for which value has to be set
            value (dictionary): value corresponding to the key

        Returns:
            integer: 0 or 1 based on value was set to corresponding key or not
        """
        try:
            value = self._redis_conn.set(key, pickle.dumps(value))
            return value
        except BaseException as e:
            logger.exception("Unable to SET value for key : " + key + " ERROR is : " + str(e))

    def redis_set_dict_value_and_expiry(self, key, value, expiry):
        """function to set a value for the key in redis. 
        Use this function if you want to set a dictionary value to a key

        Args:
            key (string): key for which value has to be set
            value (dictionary): value corresponding to the key

        Returns:
            integer: 0 or 1 based on value was set to corresponding key or not
        """
        try:
            value = self._redis_conn.set(key, pickle.dumps(value), ex=expiry)
            get_val = self.redis_get_dict_value(key)
            # logger.info("After setting value is "+str(get_val))
            return value
        except BaseException as e:
            logger.exception("Unable to SET value for key : " + key + " ERROR is : " + str(e))

    def redis_set_value_and_expiry(self, key, value, expiry):
        """function to set a value for the key in redis with expiry
       
        """
        try:
            value = self._redis_conn.set(key, value, ex=expiry)
            return value
        except BaseException as e:
            logger.exception("Unable to SET value and expiry for key : " + key + " ERROR is : " + str(e))

    def redis_delete_value(self, key):
        """function to delete a key from redis

        Args:
            key (string): key which has to be deleted from redis

        Returns:
            integer: 0 or 1 based on whether key was deleted from redis or not
        """
        try:
            value = self._redis_conn.delete(key)
            return value
        except BaseException as e:
            logger.exception("Unable to DELETE value and expiry  for key : " + key + " ERROR is : " + str(e))


    def redis_key_confirm(self, key, rd_dict={}):
        """function to confirm if key is present in the redis or not

        Returns:
            boolean: True or False based on whether the updation was successfull or not
        """
        try:

            if key in rd_dict.keys():
                key_val = rd_dict[key]
                if key == "" or key is None:
                    key_val = ""
            else:
                logger.info("******===========stage is not in redis data===========*****")
                key_val = ""

            return key_val

        except BaseException as e:
            logger.exception("redis_key_confirm: Exception!!! Failed to get data from redis" + str(e))
            return False

