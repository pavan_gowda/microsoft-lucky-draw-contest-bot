export LANG="C.UTF-8"
DATE=`/bin/date '+%Y-%m-%d-%H-%M-%S'`

FILE=log_file.log
DIR=OLD_LOGS

if [ ! -f "$FILE" ]; then
    echo "WARNING: $FILE log file not exist for the first time so creating it"
    touch $FILE
else
  if [ ! -d "$DIR" ]; then
    mkdir $DIR
  fi
    mv $(pwd)/log_file.log $(pwd)/OLD_LOGS/log_file.log-${DATE}.bck
fi

pkill -f -9 python3
export LANG=C.UTF-8
export BOT_ENV="dev"
nohup python3 main/server.py &
