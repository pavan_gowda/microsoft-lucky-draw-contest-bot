# Author : Avinash Benki
# Email : avinash@gnani.ai
# Date : 18-10-2020
# description: Dialogflow Autorun  

FROM gnanivbprod.azurecr.io/rasa:1.10.16

WORKDIR /nlp_server

COPY requirements.txt /nlp_server
RUN pip3 install -r requirements.txt

COPY . /nlp_server
RUN chmod 777 run.sh

EXPOSE 5002

ENTRYPOINT ["sh", "run.sh"]
